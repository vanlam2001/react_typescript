import React from 'react'

interface Phone{
  maSP: number;
  tenSP: string;
  manHinh: string;
  heDieuHanh: string;
  cameraTruoc: string;
  cameraSau: string;
  ram: string;
  rom: string;
  giaBan: number;
  hinhAnh: string;
}

interface detailPhoneProps{
  detail: Phone;
}

const DetailPhone: React.FC<detailPhoneProps> = (props) => {
  const {
    maSP,
    manHinh,
    heDieuHanh,
    cameraTruoc,
    cameraSau,
    ram,
    rom,
    giaBan,
    hinhAnh
  } = props.detail;
  return (
    <div className='row'>
      <div className="col-md-4">
        <h3 className='text-center'></h3>
        <img style={{width: "18rem"}} src={hinhAnh} alt="" />
      </div>
      <div className="col-8">
        <table className='table'>
        <thead>
                            <tr>
                                <td colSpan={2}>
                                    <h3>Thông số kỹ thuật</h3>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan={2}>
                                    <h3>{maSP}</h3>
                                </td>
                            </tr>
                            <tr>
                                <td>Màn hình</td>
                                <td>{manHinh}</td>
                            </tr>
                            <tr>
                                <td>Hệ điều hành</td>
                                <td>{heDieuHanh}</td>
                            </tr>
                            <tr>
                                <td>Camera trước</td>
                                <td>{cameraTruoc}</td>
                            </tr>
                            <tr>
                                <td>Camera Sau</td>
                                <td>{cameraSau}</td>
                            </tr>
                            <tr>
                                <td>RAM</td>
                                <td>{ram}</td>
                            </tr>
                            <tr>
                                <td>ROM</td>
                                <td>{rom}</td>
                            </tr>
                            <tr>
                                <td>Giá bán</td>
                                <td>{giaBan}</td>
                            </tr>
                        </thead>

        </table>
      </div>
    </div>
  )
}

export default DetailPhone;

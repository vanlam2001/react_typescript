import React from 'react'

interface Phone{
    hinhAnh: string;
    tenSP: string;
    giaBan: number;
}

interface ItemPhoneProps{
    data: Phone;
    handleProductView: (sp: Phone) => void;
}
const ItemPhone: React.FC<ItemPhoneProps> = (props) => {
    const { hinhAnh, tenSP, giaBan } = props.data
    const handleProductView = () => {
        props.handleProductView(props.data)
    }   
    return (
    <div>
         <div className="col-4 p-4">
            <div className="card border-primary h-100">
                <img className="card-img-top" src={hinhAnh} alt="" />
                <div className="card-body">
                    <h4 className="card-title">{tenSP}</h4>
                    <p className="card-text">{giaBan}</p>
                    <button onClick={handleProductView} className="btn btn-success">Xem chi tiết</button>
                </div>
            </div>
        </div>
    </div>
  )
}

export default ItemPhone;

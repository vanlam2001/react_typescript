import React from 'react'
import ItemPhone from './ItemPhone'

interface listPhoneProps {
    list: any[];
    handleProductView: (sp: any) => void;
    
}

const ListPhone: React.FC<listPhoneProps> = (props) => {
    const renderListPhone = () => {
        return props.list.map((item: any, index: number) => {
            return <ItemPhone data={item} key={index} handleProductView={props.handleProductView}/>
        })
    }
    return (
    <div className='row'>
        {renderListPhone()}
    </div>
  )
}

export default ListPhone;

import React, { useState } from 'react'
import ListPhone from './ListPhone';
import DetailPhone from './DetailPhone';
import { data_phone } from './data_phone';

interface Phone {
  maSP: number;
  tenSP: string;
  manHinh: string;
  heDieuHanh: string;
  cameraTruoc: string;
  cameraSau: string;
  ram: string;
  rom: string;
  giaBan: number;
  hinhAnh: string;
}

const Ex_Phone: React.FC = () => {
  const [listPhone] = useState<Phone[]>(data_phone);
  const [detailPhone, setDetailPhone] = useState<Phone>(data_phone[0]);
  const handleProductView = (sp:Phone ) => {
    setDetailPhone(sp);
  }
    return (
    <div>
        <ListPhone handleProductView={handleProductView} list={listPhone}/>
        <DetailPhone detail={detailPhone}/>
    </div>
  )
}

export default Ex_Phone;
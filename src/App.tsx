import './App.css';
import ConditinalRendering from './ConditinalRendering/ConditinalRendering';
import DemoProps from './DemoProps/DemoProps';
import DemoState from './DemoState/DemoState';
import EventHandling from './EventHandling/EventHandling';
import Ex_Car_Color from './Ex_Car_Color/Ex_Car_Color';
import Ex_Demo_Redux_Mini from './Ex_Demo_Redux_Mini/Ex_Demo_Redux_Mini';
import Ex_Demo_Redux_Thunk_Toolkit from './Ex_Demo_Redux_Thunk_Toolkit/Ex_Demo_Redux_Thunk_Toolkit';

import Ex_Phone from './Ex_Phone/Ex_Phone';
import Ex_Shoe_Shop_Function from './Ex_Shoe_Shop_Function/Ex_Shoe_Shop_Function';
import RenderWithMap from './RenderWithMap/RenderWithMap';

function App() {
  return (
    <div>
         {/* <EventHandling/> */}
         {/* <ConditinalRendering/> */}
         {/* <DemoState/> */}
         {/* <RenderWithMap/> */}
         {/* <Ex_Car_Color/> */}
         {/* <DemoProps/> */}
         {/* <Ex_Phone/> */}
         {/* <Ex_Shoe_Shop_Function/> */}
         {/* <Ex_Demo_Redux_Mini/> */}
         <Ex_Demo_Redux_Thunk_Toolkit/>
         
    </div>
  );
}

export default App;

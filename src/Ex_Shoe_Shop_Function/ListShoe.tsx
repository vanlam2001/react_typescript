import React from 'react'
import ItemShoe from './ItemShoe';

interface ListShoeProps{
    listShoe: any[];
    themGioHang: (sp: any) => void
}

const ListShoe: React.FC<ListShoeProps> = ({listShoe, themGioHang}) => {
  
  return (
    <div className='container p-5'>
            <div className="row p-5">
            {listShoe.map((shoe: any, index: number) => {
               return <ItemShoe key={index} shoe={shoe} themGioHang={themGioHang}/>
            })}
            </div>
    </div>
  )
}

export default ListShoe;

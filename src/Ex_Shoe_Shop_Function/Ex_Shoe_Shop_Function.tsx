import React, { useState } from 'react'
import { data_shoe } from './data_shoe';
import ListShoe from './ListShoe';
import ShoeCart from './ShoeCart';

interface Shoe {
  id: number;
  name: string;
  alias: string;
  price: number;
  description: string;
  shortDescription: string;
  quantity: number;
  image: string;
  soLuong: number;
}




const Ex_Shoe_Shop_Function: React.FC = () => {
  const [listShoe] = useState<Shoe[]>(data_shoe);
  const [shoeCart, setShoeCart] = useState<Shoe[]>([]);

  const themGioHang = (shoe: Shoe) => {
      let shoeCartClone = [...shoeCart];
      let index = shoeCartClone.findIndex((item) => {
        return item.id === shoe.id;
      });
      
      if(index === -1){
        let shoeClone = {...shoe, soLuong: 1};
        shoeCartClone.push(shoeClone);
      } else{
        shoeCartClone[index].soLuong +=1;
      }
      setShoeCart(shoeCartClone)
  }
  const xoaGioHang = (shoe: Shoe) => {
      let shoeCartClone = [...shoeCart]
      let index = shoeCartClone.filter((item) => {
        return item.id !== shoe.id
      })
      setShoeCart(index);
  }

  const chinhsoLuong = (id: number, luaChon: number): void => {
    let shoeCartClone = [...shoeCart];
    let index = shoeCartClone.findIndex((item) => {
      return item.id === id;
    });

    if (index !== -1) {
      shoeCartClone[index].soLuong += luaChon;
      setShoeCart(shoeCartClone);
    }
  };
  
  return (
    <div>
      <ListShoe listShoe={listShoe} themGioHang={themGioHang}   />
      <ShoeCart shoeCart={shoeCart} xoaGioHang={xoaGioHang} chinhsoLuong={chinhsoLuong} />
    </div>
  )
}

export default Ex_Shoe_Shop_Function;
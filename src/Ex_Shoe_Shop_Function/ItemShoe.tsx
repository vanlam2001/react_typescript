import React from 'react'

interface Shoe{
  id: number;
  name: string;
  alias: string;
  price: number;
  description: string;
  shortDescription: string;
  quantity: number;
  image: string;
}

interface ItemShoeProps{
  shoe: Shoe;
  themGioHang: (sp: Shoe) => void
}

const ItemShoe: React.FC<ItemShoeProps> = ({shoe, themGioHang}) => {
  const handleClick = (event: React.MouseEvent<HTMLAnchorElement>) => {
    event.preventDefault();
    themGioHang(shoe);
  }
  
  return (
    <div className='col-4 mb-5'>
         <img src={shoe.image} className='card-img-top' alt="" />
         <div className="card-body">
          <h5 className='card-title'>{shoe.name}</h5>
          <p className='card-text'>{shoe.price}</p>
          <a onClick={handleClick} href="" className='btn btn-primary'>Add to cart</a>
         </div>

    </div>
  )
}

export default ItemShoe

import React from 'react'

interface Shoe{
  id: number;
  name: string;
  alias: string;
  price: number;
  description: string;
  shortDescription: string;
  quantity: number;
  image: string;
  soLuong: number;
}

interface ShoeCartProps{
  shoeCart: Shoe[];
  xoaGioHang: (sp: Shoe) => void;
  chinhsoLuong: (id: number, luaChon: number) => void;
}

const ShoeCart: React.FC<ShoeCartProps> = ({shoeCart, xoaGioHang, chinhsoLuong}) => {
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>, index: number) => {
    event.preventDefault();
    const shoeToRemove = shoeCart[index];
    xoaGioHang(shoeToRemove);
  }
  const renderShoeCart = () => {
      return shoeCart.map((shoe: Shoe, index: number) => {
          return (
            <tr key={index}>
               <td>{shoe.id}</td>
               <td>{shoe.name}</td>
               <td>{shoe.price}</td>
               <td>
                <button onClick={() => chinhsoLuong(shoe.id, -1)} className='btn btn-outline-info btn-sm'>-</button>
                <strong className='mx-2'>
                  {shoe.soLuong}
                </strong>
                <button onClick={() => chinhsoLuong(shoe.id, +1)}   className='btn btn-outline-info btn-sm'>+</button>
               </td>
               <td>{shoe.price * shoe.soLuong}</td>
               <td>
                <img src={shoe.image} style={{width: '50px'}} alt="" />
               </td>
               <button onClick={(event) => handleClick(event, index)} className='btn btn-danger'>Xoá</button>
            </tr>
          )      
      })
  }
  
  return (
    <div className='container p-5'>
       <table className='table table-inverse table-inverse'>
           <thead className='thead-inverse'>
              <tr>
              <th>ID</th>
            <th>Tên Sản phẩm</th>
            <th>Gía</th>
            <th>Số lượng</th>
            <th>Tổng</th>
            <th>Hình ảnh</th>
            <th>Chức năng</th>
              </tr>
           </thead>
           <tbody>
            {renderShoeCart()}
           </tbody>
       </table>
    </div>
  )
}

export default ShoeCart;

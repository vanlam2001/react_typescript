import React, { useState } from 'react'

const DemoState: React.FC = () => {
  const [userName, setUserName] = useState('alice');
  const [ageUserName, setAgeUserName] = useState(18);

  const handleChangeUserName = () => {
     let name;
     if(userName === 'bob'){
         name = 'alice';
     } else{
        name = 'bob';
     }

     setUserName(name);
  }

  const handleChangeAge = () => {
    let age;
    if(ageUserName === 20){
        age = 18;
    } else{
        age = 20;
    }
    setAgeUserName(age);
  }
    return (
    <div>
        <h2>DemoString</h2>
        <h3 className={userName == 'bob' ? 'text-danger' : 'text-primary'}>{userName}</h3>
        <button onClick={handleChangeUserName} className='btn btn-warning'>Change username</button>

        <h2>DemoNumber</h2>
        <h3>{ageUserName}</h3>
        <button onClick={handleChangeAge} className='btn btn-danger'>Change Age</button>

    </div>
  )
}

export default DemoState;

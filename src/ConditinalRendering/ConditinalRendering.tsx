import React, { useState } from 'react'

const ConditinalRendering: React.FC = () => {
    const [isLogin, setIsLogin] = useState(false);
    
    const handleLogin = (): void => {
        console.log('before', isLogin);
        setIsLogin(true);
        console.log('after', isLogin);
    }

    const handleLogout = (): void => {
        console.log('before', isLogin);
        setIsLogin(false);
        console.log('after', isLogin);
    }

    const renderContentButton = () => {
        if(isLogin){
         return (
            <button onClick={handleLogout} className='btn btn-danger'>Logout</button>
         ); 
        } else{
            return(
                <button onClick={handleLogin} className='btn btn-success'>Login</button>
            )
        }
    }
    return (
        <div>
            <h2>ConditinalRendering</h2>
             {renderContentButton()}
        </div>
    )
}

export default ConditinalRendering;

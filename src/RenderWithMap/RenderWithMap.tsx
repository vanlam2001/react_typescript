import React, { useState } from 'react'
import { dataFoodList } from './dataFoodList';

const RenderWithMap: React.FC = () => {
   const [foodList] = useState(dataFoodList);

   const renderFoodListComponent = () => {
       return foodList.slice(0, 12).map((item, index) => {
          return (
            <div className='card' style={{width: '18rem'}} key={index}>
                <div className='card-body'>
                    <h5 className='card-title'>{item.tenMon}</h5>
                    <p className='card-text'>{item.giaMon}</p>
                    <a className='btn btn-primary' href="">Thêm vào giỏ hàng</a>
                </div>
            </div>
          )
       })
   }
    return (
    <div>
        {renderFoodListComponent()}
    </div>
  )
}

export default RenderWithMap;

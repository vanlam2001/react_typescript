import { configureStore } from "@reduxjs/toolkit";
import { PersonSlice } from "./features/personSlice";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";

export const RootStore = configureStore({
    reducer:{
        person: PersonSlice.reducer
    }
})

export const useAppDispatch:()=> typeof RootStore.dispatch=useDispatch;
export const useAppSelector:TypedUseSelectorHook<ReturnType<typeof RootStore.getState>>=useSelector;

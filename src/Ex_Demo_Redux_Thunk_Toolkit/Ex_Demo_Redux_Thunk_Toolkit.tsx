import React from 'react'
import Add from './Add'
import List from './List'

const Ex_Demo_Redux_Thunk_Toolkit: React.FC = () => {
  return (
    <div>
        <Add/>
        <List/>
    </div>
  )
}

export default Ex_Demo_Redux_Thunk_Toolkit

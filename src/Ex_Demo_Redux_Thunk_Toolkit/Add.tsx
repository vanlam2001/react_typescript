import React, { useRef } from 'react'
import { useAppDispatch } from './store/store';
import { addPerson } from './store/features/personSlice';

const Add: React.FC = () => {
  const name=useRef<string>("");
  const dispatch = useAppDispatch();
    return (
    <div>
        <form className='border rounded-md p-2 shadow-md m-2'>
            <label htmlFor="">Person Name:</label>
             <input onChange={(e) => (name.current = e.target.value)} className='border rounded-md p-2 mx-2'/>
             <button onClick={()=> dispatch(addPerson({name:name.current}))} className='btn btn-danger'>Add</button>
        </form>
    </div>
  )
}

export default Add
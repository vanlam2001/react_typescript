import React, { useState } from 'react';

const Ex_Car_Color = () => {
    const [imgSrc, setImgSrc] = useState('./img/red-card.jpg');

    const handleChangeColor = (color) => {
        let src = `./img/${color}-card.jpg`;
        setImgSrc(src);
    };

    return (
        <div className='row'>
            <img className='col-4' src={imgSrc} alt='' />
            <div>
                <button className='btn btn-danger' onClick={() => handleChangeColor('red')}>
                    Red
                </button>
                <button className='btn btn-dark mx-5' onClick={() => handleChangeColor('black')}>
                    Black
                </button>
                <button className='btn btn-secondary' onClick={() => handleChangeColor('silver')}>
                    Silver
                </button>
            </div>
        </div>
    );
};

export default Ex_Car_Color;
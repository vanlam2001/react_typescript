import React from 'react'

interface UserInfoProps{
    hoTen: string;
    handleChangeUserName:() => void;
    tuoi: number;
    handleChangeAge: () => void;
}
const UserInfo: React.FC <UserInfoProps> = (props) => {
    console.log('props', props)
    return (
    <div>UserInfo
        <h2>UserName: {props.hoTen}</h2>
        <h3>Age: {props.tuoi}</h3>
        <button onClick={props.handleChangeUserName}  className='btn btn-success'>Change username</button>
        <button onClick={props.handleChangeAge}  className='btn btn-danger'>Change Age</button>
    </div>
  )
}

export default UserInfo

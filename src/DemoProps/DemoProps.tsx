import React, { useState } from 'react';
import UserInfo from './UserInfo';

interface UserInfoProps {
  hoTen: string;
}

const DemoProps: React.FC = () => {
  const [userName, setUserName] = useState<string>('alice');
  const [ageUserName, setAgeUserName] = useState<number>(18);

  const handleChangeUserName = (): void => {
    let name: string;
    if (userName === 'bob') {
      name = 'alice';
    } else {
      name = 'bob';
    }
    setUserName(name);
  };

  const handleChangeAge = () :void => {
    let age: number;
    if(ageUserName === 20){
       age = 18
    } else {
      age = 20;
    }
    setAgeUserName(age);
  }





  return (
    <div>
      <UserInfo hoTen={userName}
        handleChangeUserName={handleChangeUserName}
        tuoi={ageUserName}
        handleChangeAge={handleChangeAge}
      />
    </div>
  );
};

export default DemoProps;
import React from 'react'

const EventHandling: React.FC = () => {
    const handleLogin = (): void =>{
        console.log('yes');
    }
    const handleSayHelloWithName = (username: string, age: number): void => {
          console.log('hello',username, age);
    }
    return (
    <div>
        <h2>EventHandling</h2>
        <button onClick={handleLogin} className='btn btn-success'>Login</button>
        <button onClick={() => {handleSayHelloWithName('Alice', 18)}}  className='btn btn-danger'>Say hello with me</button>
    </div>
  )
}

export default EventHandling;

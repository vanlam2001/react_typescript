interface State{
    soLuong: number;
}

const initialValue: State = {
    soLuong: 100,
};

type Action = {type: 'TANG_SO_LUONG'} | {type: 'GIAM_SO_LUONG'};


export const numberReducer = (state: State = initialValue, action: Action): State => {
    if(action.type === 'TANG_SO_LUONG'){
         console.log('Xử lý tăng số lượng')
         state.soLuong = state.soLuong + 2;
         return {...state};
    } else if(action.type === 'GIAM_SO_LUONG'){
         console.log('Xử lý giảm số lượng')
         state.soLuong = state.soLuong - 2;
         return {...state};
    }
    else{
        return state
    }

}
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

interface State {
  soLuong: number;
}

interface Props {
  soLuong: number;
  handleTangSoLuong: () => void;
  handleGiamSoLuong: () => void;
}

const Ex_Demo_Redux_Mini: React.FC<Props> = ({ soLuong, handleTangSoLuong, handleGiamSoLuong }) => {
  console.log("props", { soLuong });
  return (
    <div>
      <button onClick={handleGiamSoLuong} className='btn btn-danger'>-</button>
      <strong className='mx-5'>{soLuong}</strong>
      <button className='btn btn-success' onClick={handleTangSoLuong}>+</button>
    </div>
  );
}

const mapStateToProps = (state: State) => {
  return {
    soLuong: state.soLuong
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    handleTangSoLuong: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };
      dispatch(action);
    },
    handleGiamSoLuong: () => {
      let action = {
        type: "GIAM_SO_LUONG",
      }
      dispatch(action);
    }

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Ex_Demo_Redux_Mini);